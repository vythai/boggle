// This is the CPP file you will edit and turn in. (TODO: Remove this comment!)

#include "boggle.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "console.h"
#include "filelib.h"
#include "grid.h"
#include "lexicon.h"
#include "random.h"
#include "set.h"
#include "shuffle.h"
#include "simpio.h"
#include "strlib.h"
#include "vector.h"
#include "gui.h"
using namespace std;

// function prototype declarations
void intro();


bool checkUserWord(string word, Lexicon& dictionary, Set<string>& playerWordList, Grid<char>& board, int& playerScores){

    if(!dictionary.contains(word)) {
        cout<< "That word is not found in the dictionary."  << endl;
        gui::playSound("not.wav");
        return false;
    }
    if(word.size()<BOARD_SIZE) {
        cout<< "The word must have at least 4 letters." << endl;
        gui::playSound("not-fooling-anyone.wav");
        return false;
    }

    if(playerWordList.contains(word)) {
        cout<< "You have already found that word."  << endl;
        gui::playSound("whoops.wav");
        return false;
    }
    if(!humanWordSearch(board, word)) {
        cout<< "That word can't be formed on this board."  << endl;
        gui::playSound("denied.wav");
        cout << "Your words: " << playerWordList << endl;
        cout << "Your score: " << playerScores << endl;
        return false;
    }

    playerWordList.add(word);
    gui::recordWord("human", word);
    int sound = randomInteger(1,3);
    switch (sound)
    {
        case 1: gui::playSound("excellent.wav");
        case 2: gui::playSound("tinkerbell.wav");
        case 3: gui::playSound("oh-really.wav");
    }
    return true;
}
bool checkComputerWord(string word){
    if(word.size()<BOARD_SIZE) {
        return false;
    }
    return true;
}
void calculateScore(int& scores, string word) {
    if(word.size() == 4) {
        scores++;
    } else if(word.size()== 5) {
        scores = scores + 2;
    } else if(word.size() == 6) {
        scores= scores + 3;
    } else if(word.size() == 7) {
        scores = scores + 5;
    } else {
        scores = scores + 11;
    }
}

void shake(Grid<char>& board) {
    Vector<string> tempCubes = LETTER_CUBES;
    int diceCounter = 0;
    shuffle(tempCubes);
    gui::playSound("dice-rattle.wav");
    for(int r = 0; r < BOARD_SIZE; r++) {
        for(int c = 0; c < BOARD_SIZE; c++) {
            board[r][c] = shuffle(tempCubes[diceCounter])[0];
            cout << board[r][c];
            diceCounter++;
            pause(30);
        }
        cout << endl;
    }
}

void userBoard(Grid<char>& board){
    string userInput;
    do {
        userInput = getLine("Type the 16 letters on the board:");
        if(userInput.size() != BOARD_SIZE * BOARD_SIZE) {
            cout << "Invalid board string. Try again." << endl;
        }
    } while(userInput.size() != BOARD_SIZE * BOARD_SIZE);
    userInput = toUpperCase(userInput);

    int diceCounter = 0;
    for(int r = 0; r < BOARD_SIZE; r++) {
        for(int c = 0; c < BOARD_SIZE; c++) {
            board[r][c] = userInput[diceCounter];
            cout << board[r][c];
            diceCounter++;
        }
        cout << endl;
    }

}
void ask(Grid<char>& board) {
    if (!getYesOrNo("Generate a random board?")){
        userBoard(board);
    } else {
        shake(board);
    }

}

bool humanWordSearch(Grid<char>& board, string word, Grid<bool>& visit, int row, int col) {
    // TODO: write this function
    if(word == "") {
        return true;
    }

    if(visit[row][col]) {
        return false;
    }
    if(word[0]==board[row][col]) {
        gui::setHighlighted(row, col, true);
        visit[row][col] = true;

        for (int r = row - 1; r <= row + 1; r++) {
            for (int c = col - 1; c <= col + 1; c++) {
                if (board.inBounds(r, c)) {
                    if(humanWordSearch(board, word.substr(1), visit, r, c)) {
                        //gui::setHighlighted(r, c, true);
                        return true;
                    }
                }
            }
        }
    }

    visit[row][col] = false;
    gui::setHighlighted(row, col, false);
    return false;
}

bool humanWordSearch(Grid<char>& board, string word) {
    // TODO: write this function
    Grid<bool> visit(BOARD_SIZE, BOARD_SIZE);

    for(int i = 0; i< BOARD_SIZE; i++) {
        for(int j = 0; j< BOARD_SIZE; j++) {
            visit[i][j]= false;
        }
    }

    for(int i = 0; i< BOARD_SIZE; i++) {
        for(int j = 0; j< BOARD_SIZE; j++) {
            if(humanWordSearch(board, word, visit, i, j)) {
                return true;
                gui::playSound("tweetle.wav");
            }
        }
    }
    return false;
}


void computerWordSearch(Set<string>& words, Grid<bool>& visit, Grid<char>& board, Lexicon& dictionary, Set<string>& humanWords, int row, int col, string word) {
    // TODO: write this function
    if(checkComputerWord(word) && dictionary.contains(word) && !humanWords.contains(word)) {
        words.add(word);
        gui::recordWord("computer", word);
    }

    for (int r = row - 1; r <= row + 1; r++) {
        for (int c = col - 1; c <= col + 1; c++) {
            if (board.inBounds(r, c) && !visit[r][c]) {
                word = word + board[r][c];
                if(dictionary.containsPrefix(word)) {
                    visit[r][c] = true;
                    computerWordSearch(words, visit, board, dictionary, humanWords, r, c, word);
                    visit[r][c] = false;
                    word.erase(word.size()-1);
                } else {
                    word.erase(word.size()-1);
                }
            }
        }
    }
}

Set<string> computerWordSearch(Grid<char>& board, Lexicon& dictionary, Set<string>& humanWords) {
    // TODO: write this function

    Set<string> words;
    Grid<bool> visit(BOARD_SIZE, BOARD_SIZE);
    for(int i = 0; i< BOARD_SIZE; i++) {
        for(int j = 0; j< BOARD_SIZE; j++) {
            visit[i][j]= false;
        }
    }

    string wordFound;
    for(int i = 0; i< BOARD_SIZE; i++) {
        for(int j = 0; j<BOARD_SIZE; j++) {
            wordFound = charToString(board[i][j]);
            visit[i][j] = true;
            computerWordSearch(words, visit, board, dictionary, humanWords, i, j, wordFound);
            visit[i][j] = false;

        }
    }
    return words;
}

// Prints a welcome message that introduces the program to the user.
void intro() {
    cout << "Welcome to CS 106B Boggle!" << endl;
    cout << "This game is a search for words on a 2-D board of letter cubes." << endl;
    cout << "The good news is that you might improve your vocabulary a bit." << endl;
    cout << "The bad news is that you're probably going to lose miserably to" << endl;
    cout << "this little dictionary-toting hunk of silicon." << endl;
    cout << "If only YOU had 16 gigs of RAM!" << endl;
    cout << endl;
    getLine("Press Enter to begin the game ...");
    cout << endl;
}

void process(Grid<char>& board, Lexicon& dictionary) {
    gui::initialize(BOARD_SIZE, BOARD_SIZE);
    Set<string> playerWordList;
    int playerScores=0;

    ask(board);

    gui::labelCubes(board);

    cout << endl;

    cout << "It's your turn!" << endl;

    string userWord = "START";
    cout << "Your words: " << playerWordList << endl;
    cout << "Your score: " << playerScores << endl;
    userWord = getLine("Type a word (or Enter to stop):");

    while(userWord != ""){
        userWord = toUpperCase(userWord);
        if (checkUserWord(userWord, dictionary, playerWordList, board, playerScores)) {
            cout << "You found a new word! \"" ;
            cout << userWord << "\"" << endl << endl;
            calculateScore(playerScores, userWord);
            cout << "Your words: " << playerWordList << endl;
            cout << "Your score: " << playerScores << endl;
            gui::setScore("human", playerScores);
        }
        gui::clearHighlighting();
        userWord = getLine("Type a word (or Enter to stop):");

    }

    cout << endl;
    cout << "It's my turn!" << endl;


    Set<string> computerWords = computerWordSearch(board, dictionary, playerWordList);
    int computerScores =0;

    for(string word : computerWords) {
        calculateScore(computerScores, word);
    }
    cout << "My words: " << computerWords << endl;
    cout << "My score: " << computerScores << endl;
    if(computerScores>playerScores) {
        cout << "Ha ha ha, I destroyed you. Better luck next time, puny human!" << endl << endl;
        gui::playSound("tinkerbell.wav");
    } else {
        cout << "WOW, you defeated me! Congratulations!" << endl << endl;
    }

    gui::setScore("computer", computerScores);
}

int main() {
    gui::initialize(BOARD_SIZE, BOARD_SIZE);
    intro();
    Grid<char> board(BOARD_SIZE, BOARD_SIZE);

    Lexicon dictionary(DICTIONARY_FILE);


    process(board, dictionary);
    //if(getYesOrNo("Play again?"))
   // Play again? N
    while(getYesOrNo("Play again?")) {
        cout << endl;
        process(board, dictionary);
    }
    cout << "Have a nice day." << endl;

    return 0;
}



